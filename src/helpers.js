import aws from 'aws-sdk'

import config from '../config'

const s3 = new aws.S3()

export const sendFileToS3 = (file, name) => {
  const params = {
    Bucket: config.bucket,
    Key: name,
    Body: file,
    ACL: 'public-read',
  }

  return s3.upload(params, (err, data) => {
    if (err) console.log('error', err)

    return data
  }).promise()
}

export default { sendFileToS3 }
