import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'

const NewSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      index: true,
      required: true,
    },
    name: {
      type: String,
      trim: true,
      required: true,
    },
    description: {
      type: String,
    },
    imageId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Image',
    },
    imageUrl: {
      type: String,
    },
  },
  { collection: 'news' },
)

NewSchema.plugin(timestamps)

export default mongoose.model('New', NewSchema)
