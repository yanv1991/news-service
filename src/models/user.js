import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: true,
  },
  name: {
    first: {
      type: String,
      trim: true,
      required: true,
    },
    last: {
      type: String,
      trim: true,
      required: true,
    },
  },
  password: {
    type: String,
    required: true,
  },
}, { collection: 'users' })

UserSchema.plugin(timestamps)

export default mongoose.model('User', UserSchema)
