import mongoose from 'mongoose'
import timestamps from 'mongoose-timestamp'

const ImageSchema = new mongoose.Schema(
  {
    filename: {
      type: String,
      trim: true,
      required: true,
    },
  },
  { collection: 'images' },
)

ImageSchema.plugin(timestamps)

export default mongoose.model('Image', ImageSchema)
