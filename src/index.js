import restify from 'restify'
import mongoose from 'mongoose'
import os from 'os'
import corsMiddleware from 'restify-cors-middleware'
import rjwt from 'restify-jwt-community'
import '@babel/polyfill'

import config from '../config'

const server = restify.createServer({
  name: config.name,
  version: config.version,
})
const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['authorization', 'Access-Control-Allow-Origin'],
})

server.use(
  restify.plugins.bodyParser({
    mapParams: true,
    mapFiles: true,
    keepExtensions: true,
    uploadDir: os.tmpdir(),
  }),
)

if (!config.jwt.secret) {
  throw Error('Plese make sure you have the .env file in the root dir with the required config.')
}

server.use(rjwt(config.jwt).unless({
  path: [
    '/users/auth',
    { url: '/news', methods: ['GET'] },
  ],
}))

server.pre(cors.preflight)
server.use(cors.actual)
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())


server.listen(config.port, () => {
  /**
   * Connect to MongoDB via Mongoose
   */
  const opts = {
    promiseLibrary: global.Promise,
    auto_reconnect: true,
    reconnectInterval: 1000,
    config: {
      autoIndex: true,
    },
    useNewUrlParser: true,
  }

  mongoose.Promise = opts.promiseLibrary
  mongoose.connect(config.db.uri, opts)

  const db = mongoose.connection

  db.on('error', (err) => {
    console.log('err', err)
    if (err.message.code === 'ETIMEDOUT') {
      console.log(err)
      mongoose.connect(config.db.uri, opts)
    }
  })

  db.once('open', () => {
    require('./routes/user')(server) // eslint-disable-line global-require
    require('./routes/new')(server) // eslint-disable-line global-require
    require('./routes/image')(server) // eslint-disable-line global-require
    console.log(`Server is listening on port ${config.port}`)
  })
})
