import status from 'http-status-codes'

import New from '../models/new'
import { sendFileToS3 } from '../helpers'

module.exports = (server) => {
  /**
   * Create
   */
  server.post('/news', async (req, res, next) => {
    let imageResponse

    if (req.params.image) {
      imageResponse = await sendFileToS3(req.params.image, req.files.image.name)
    }

    const data = {
      ...req.body,
      imageUrl: imageResponse ? imageResponse.Location : '',
    }

    New.create(data)
      .then((item) => {
        res.send(status.OK, item)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  /**
   * List
   */
  server.get('/news', (req, res, next) => {
    // const limit = parseInt(req.query.limit, 10) || 10
    // default limit to 10 docs

    const skip = parseInt(req.query.skip, 10) || 0
    // default skip to 0 docs

    const query = req.params || {}

    // remove skip and limit from data to avoid false querying
    delete query.skip
    delete query.limit

    New.find(query)
      .populate('user')
      .skip(skip)
      // TODO: remove limit since there is not pagination .limit(limit)
      .then((news) => {
        res.send(status.OK, news)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  /**
   * Get
   */
  server.get('/news/:newId', (req, res, next) => {
    New.findOne({ user: req.params.user, _id: req.params.todoId })
      .then((todo) => {
        res.send(status.OK, todo)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  /**
   * Update
   */
  server.put('/news/:newId', async (req, res, next) => {
    let imageResponse

    if (req.params.image) {
      imageResponse = await sendFileToS3(req.params.image, req.files.image.name).catch((err) => {
        console.log('err', err)
      })
    }

    const data = imageResponse ? {
      ...req.body,
      imageUrl: imageResponse.Location,
    } : req.body

    New.findOneAndUpdate({ _id: req.params.newId }, data, { new: true })
      .populate('user')
      .then((item) => {
        res.send(status.OK, item)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  /**
   * Delete
   */
  server.del('/news/:newId', (req, res, next) => {
    New.findOneAndRemove({ _id: req.params.newId })
      .then(() => {
        res.send(status.NO_CONTENT)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })
}
