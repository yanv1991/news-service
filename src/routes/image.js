import status from 'http-status-codes'

import { sendFileToS3 } from '../helpers'

module.exports = (server) => {
  /**
   * Create
   */
  server.post('/upload', async (req, res, next) => {
    const data = await sendFileToS3(req.params.image, req.files.image.name)

    res.send(status.OK, data)
    next()
  })
}
