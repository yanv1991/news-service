import status from 'http-status-codes'
import jwt from 'jsonwebtoken'

import User from '../models/user'
import config from '../../config'

module.exports = (server) => {
  /**
   * List
   */
  server.get('/users', (req, res, next) => {
    const limit = parseInt(req.query.limit, 10) || 10
    // default limit to 10 docs

    const skip = parseInt(req.query.skip, 10) || 0
    // default skip to 0 docs

    const query = req.query || {}

    // remove skip and limit from query to avoid false querying
    delete query.skip
    delete query.limit

    User.find(query)
      .skip(skip)
      .limit(limit)
      .then((users) => {
        res.send(status.OK, users)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  /**
   * Create
   */
  server.post('/users', (req, res, next) => {
    const data = req.body || {}

    User.create(data)
      .then((task) => {
        res.send(status.OK, task)
        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })

  server.post('/users/auth', (req, res, next) => {
    const data = req.body || {}

    User.findOne(data)
      .then((user) => {
        if (!user) {
          res.send(status.UNAUTHORIZED, { err: 'Login Failed' })
          next()
        }
        // creating jsonwebtoken using the secret from config.json
        const token = jwt.sign(user.toObject(), config.jwt.secret, {
          expiresIn: '15m', // token expires in 15 minutes
        })
        // retrieve issue and expiration times
        const { iat, exp } = jwt.decode(token)

        res.send(status.OK, { iat, exp, token })

        next()
      })
      .catch((err) => {
        res.send(status.INTERNAL_SERVER_ERROR, err)
      })
  })
}
