require('dotenv/config')

module.exports = {
  name: 'news-service',
  version: '0.0.1',
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3001,
  db: {
    uri: `mongodb+srv://${process.env.USR}:${process.env.PASS}@cluster0-zsi9a.mongodb.net/${process.env.DB}?retryWrites=true`,
  },
  bucket: process.env.BUCKET_NAME || '',
  jwt: { secret: process.env.MY_SECRET } || { secret: '' },
}
